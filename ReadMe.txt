###### INFO ######



## INSTALLATION ##

You can build and install Snort with AnomalyDetection from sources or install without building from rpm package:

* Installation from rpm package:
    - Required files and applications:
        1. daq-0.6.1-1.i386.rpm (http://www.snort.org/snort-downloads?)
        2. libdnet-1.12.tgz (http://libdnet.googlecode.com/files/libdnet-1.12.tgz)
        3. snort-2.9.2.3-1.i386.rpm (http://cdn.bitbucket.org/AnomalyDetection/preprocessor/downloads/snort-2.9.2.3-1.i386.rpm)
    
    - Installation process:
        1. Go to directory where you have saved all of files and applications from the list above 
           'cd /path/to/directory/' (for the purposes of this document we will use name 'home/').
        2. Install daq from CLI typing 'rpm -i name_of_package.rpm'
        3. Extract libdnet-1.12.tgz, you can do that by typing in CLI 'tar zxvf name_of_archive.tgz' next go to
           libdnet directory 'cd libdnet-1.12' and install it by typing './configure && make && make install'
        4. From the '/home' directory install snort using the command 'rpm -i packet_name.rpm'

* Installation from sources package:
    - Required files and applications
        1. daq-0.6.1.tar.gz (http://www.snort.org/snort-downloads?)
        2. libdnet-1.12.tgz (http://libdnet.googlecode.com/files/libdnet-1.12.tgz)
        3. snort-2.9.1.tar.gz (http://www.snort.org/snort-downloads?)
        4. all AD project files (https://bitbucket.org/AnomalyDetection/preprocessor)
		
    - Installation process:
        1. Go to directory where you have saved all of files and applications from the list above 
		   'cd /path/to/directory/' (for the purposes of this document we will use name 'home/').
        2. Extract all the archives using the command syntax 'tar zxvf file_name.tgz' or 'tar zxvf file_name.tgz.gz'
        3. Go to daq sources directory 'cd /home/daq-0.6.1' and use command './configure && make && make install'
        4. Install libdnet the same as daq, go to libdnet sources directory 'cd /home/libdnet-1.12' and use command 
           './configure && make && make install'
        5. Before you install snort copy spp_anomalydetection.c and spp_anomalydetection.h from '/home/preprocessor/src/preprocessors/' 
           directory to '/home/snort-x.x.x/src/preprocessors', you can to that by typing following command in CLI 
           'cp /home/preprocessor/src/preprocessors/spp_anomalydetection.* /home/snort-x.x.x/src/preprocessors'. 
           Next you need to modify plugbase.c (you can find them in '/home/snort-2.9.1/src/'), in section '/* built-in preprocessors */' 
           add header :
		   
                 #include "preprocessors/spp_anomalydetection.h"
		
           and in function 'void RegisterPreprocessors(void)' add :
		   
                 SetupAnomalyDetection();

           Update Makefile.in you find them in '/home/snort-x.x.x/src/preprocessors/', in the end of 'libspp_a_SOURCES' section add :
		   
                 spp_ anomalydetection.c spp_anomalydetection.h
		   
           and in the end of am_libspp_a_OBJECTS add :
		   
                 spp_anomalydetection.$(OBJEXT)
				
           Next you need to add to file /home/snort-x.x.x/src/generators.h all definitions from 'home/preprocessor/src/generators.h'.
		
        6. Go to snort-x.x.x directory 'cd /home/snort-x.x.x' and build them by typing following command in CLI :
		   
                 './configure --disable-reload && make && make install' 
           	   
#### START-UP ####

After successful installation you can run snort by typing following command in CLI 'snort -c path/to/snort.conf -h home_net/mask', 
but you need to know that anomalydetection will not work until you add following row to config file :

preprocessor AnomalyDetection: /*Place options here, You will find out how to configure preprocessor in next chapter 
called CONFIGURATIONS*/

Remember to add all rules from '/home/preprocessor/preproc_rules/preprocessor.rules' to yours 'preprocessor.rules' file and include 
them in your configuration.

# CONFIGURATIONS #

- ProfilePath - after this keyword put full path to profile file (default: "/etc/profile.txt")
 
- LogPath -  after this keyword put full path to directory which contains logs files. (default: "/var/log/snort")

- alert - if is set preprocessor will be able to report anomalies

- log - if is set preprocessor will be able to log network traffic to file

- time - after this keyword put interval (with what time intervals it will log the traffic) (default: 600 seconds)

example:

preprocessor AnomalyDetection: ProfilePath /etc/profile.txt LogPath /var/log/snort alert log time 60 


File format with proffile:

col. 1. Date in format dd-mm-yy (ISO 8601)
col. 2. Day of week (Monday = 1 ... Sunday = 7)
col. 3. Time in format hh:mm:ss
col. 4. Log interval
col. 5...n	MIN / MAX value for each counter
